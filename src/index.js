import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom'
import { Provider } from 'react-redux'

import Store from './store'
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const store = Store()

const routes = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <div>
          <Route path="/" component={App} exact />
        </div>
      </BrowserRouter>
    </Provider>
  )
}

ReactDOM.render(routes(), document.querySelector('#root'));
registerServiceWorker();
